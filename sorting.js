//sortArray function for sorting the array
const sortArray = (originalArray) => {
    if (originalArray.length <=1) {
        return originalArray;
    } else {
        let left = [];
        let right = [];
        let pivot = originalArray.pop(); //Will give the last item from the array
        let length = originalArray.length;

        for (let i=0; i<length; i++) {
            if (originalArray[i] <= pivot) {
                left.push(originalArray[i]);
            } else {
                right.push(originalArray[i]);
            }
        }

        return [...sortArray(left), pivot, ...sortArray(right)]
    }
}

let originalArray = [10, 8, 1, 6, 4, 2, 7, 3, 9, 5];
let sortedArray = sortArray(originalArray);
console.log("sorted array is:", sortedArray)

export { sortArray } 
