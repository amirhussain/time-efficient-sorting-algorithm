# time-efficient-sorting-algorithm

Quicksort Algorithm Implementation

Quicksort algorithm follows the "divide and conquer" method. 

`sortArray` function takes an array of unsorted numbers as input. If the array has only one element, sorting can be ignored and the same array can be returned.

We have to find the "pivot" element in the array. Start a pointer from the left at the first element of the array. Start a point from right at the end element of the array. Move the left pointer to right if the element at left pointer is less than the pivot and move the right pointer to left if the element at right pointer is greater than the pivot element. This is achieved with left and right arrays in the function. Again we sort the left and right arrays using the `sortArray` function using recursion. 

To run the test case follow the below steps
go to root directory ( cd time-efficient-sorting-algorithm) and run the following commands
1. `npm install`
2. `npm test`


