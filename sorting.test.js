const { sortArray } = require("./sorting")

let unsorted = [10, 8, 1, 6, 4, 2, 0, 7, 3, 9, 5];
let sorted = [0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10];

test('check for an empty array', () => {
    expect(unsorted.length).toBeGreaterThan(1)
})

test('To check whether sorted function is sorting an array or not', () => {
    expect(sortArray(unsorted)).toEqual(sorted)
})